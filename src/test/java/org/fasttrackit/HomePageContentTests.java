package org.fasttrackit;

import org.fasttrackit.pages.HomePageContent;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.open;
import static org.fasttrackit.DataSource.*;
import static org.testng.Assert.*;

@Test
public class HomePageContentTests {

    HomePageContent homePageContent;

    @BeforeTest
    public void openPage() {
        open(HOME_PAGE);
        homePageContent = new HomePageContent();
    }

    @AfterTest
    public void cleanup() {
        homePageContent.resetApp();
    }

    @Test
    public void verify_search_input_field() {
        assertTrue(homePageContent.isSearchFieldDisplayed(), "The search field must be displayed on the" +
                "home page");
        assertEquals(homePageContent.searchFieldPlaceholder(), SEARCH_INPUT_PLACEHOLDER, "The input field's " +
                "placeholder has to be Search");
    }

    @Test
    public void verify_search_button(){
        assertTrue(homePageContent.isSearchButtonDisplayed(), "The search button has to be displayed next to the " +
                "search input field");
        assertEquals(homePageContent.searchButtonLabel(), SEARCH_BUTTON_LABEL,"The search buttons label must be" +
                "Search" );
    }

    @Test
    public void verify_sort_field(){
        assertTrue(homePageContent.isSortElementDisplayed(), "The sorting element has to be displayed on the " +
                "home page");
        assertEquals(homePageContent.defaultSorting(), DEFAULT_SORT_OPTION, "Default sort option has to be 0" +
                "(Sort by name (A to Z))");
    }

    @Test (dependsOnMethods = "verify_sort_field")
    public void verify_sort_products_functionality(){
        String firstProduct = homePageContent.firstProduct();
        assertTrue(homePageContent.isSortElementDisplayed(), "The sorting element has to be displayed on the " +
                "home page");
        homePageContent.clickSort();
        homePageContent.selectLowToHighSort();
        String firstProductAfterSort = homePageContent.firstProduct();
        assertNotEquals(firstProduct, firstProductAfterSort, "The first product can't be the same if the sort works");
    }
}
