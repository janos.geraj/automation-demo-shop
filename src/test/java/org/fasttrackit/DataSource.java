package org.fasttrackit;

import org.fasttrackit.pages.Product;
import org.testng.annotations.DataProvider;

public class DataSource {
    public static final String HOME_PAGE = "https://fasttrackit-test.netlify.app/";
    public static final String CART_PAGE = "https://fasttrackit-test.netlify.app/#/cart";
    public static final String WISHLIST_PAGE = "https://fasttrackit-test.netlify.app/#/wishlist";
    public static final String BASE_URL = "https://fasttrackit-test.netlify.app/#/";

    public static final String HELLO_GUEST_GREETINGS_MESSAGE = "Hello guest!";
    public static final String LOGIN_MODAL_HEADER_TEXT = "Login";
    public static final String HELP_MODAL_HEADER_TEXT = "Help";
    public static final String SEARCH_INPUT_PLACEHOLDER = "Search";
    public static final String SEARCH_BUTTON_LABEL = "Search";
    public static final String DEFAULT_SORT_OPTION = "0";

    public static final String AWESOME_GRANITE_CHIPS_PRODUCT_ID = "1";
    public static final String AWESOME_METAL_CHAIR_PRODUCT_ID = "3";
    public static final String PRACTICAL_WOODEN_BACON = "4";
    public static final String REFINED_FROZEN_MOUSE = "0";
    public static final String GORGEOUS_SOFT_PIZZA = "9";
    public static final String AWESOME_SOFT_SHIRT = "5";

    public static final Product AWESOME_GRANITE_CHIPS_PRODUCT = new Product(AWESOME_GRANITE_CHIPS_PRODUCT_ID, "$15.99");
    public static final Product AWESOME_METAL_CHAIR_PRODUCT = new Product(AWESOME_METAL_CHAIR_PRODUCT_ID, "$15.99");
    public static final Product PRACTICAL_WOODEN_BACON_PRODUCT = new Product(PRACTICAL_WOODEN_BACON, "$29.99");
    public static final Product REFINED_FROZEN_MOUSE_PRODUCT = new Product(REFINED_FROZEN_MOUSE, "$9.99");
    public static final Product GORGEOUS_SOFT_PIZZA_PRODUCT = new Product(GORGEOUS_SOFT_PIZZA, "$19.99");
    public static final Product AWESOME_SOFT_SHIRT_PRODUCT = new Product(REFINED_FROZEN_MOUSE, "$9.99");

    @DataProvider(name = "getProductData")
    public static Object[][] getProductData() {
        Object[][] products = new Object[6][];
        products[0] = new Object[]{AWESOME_GRANITE_CHIPS_PRODUCT};
        products[1] = new Object[]{AWESOME_METAL_CHAIR_PRODUCT};
        products[2] = new Object[]{PRACTICAL_WOODEN_BACON_PRODUCT};
        products[3] = new Object[]{REFINED_FROZEN_MOUSE_PRODUCT};
        products[4] = new Object[]{GORGEOUS_SOFT_PIZZA_PRODUCT};
        products[5] = new Object[]{AWESOME_SOFT_SHIRT_PRODUCT};
        return products;
    }
}
