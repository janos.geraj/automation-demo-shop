package org.fasttrackit;
import org.fasttrackit.pages.Footer;
import org.fasttrackit.pages.Header;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.open;
import static org.fasttrackit.DataSource.*;
import static org.testng.Assert.*;

@Test
public class FooterTests {

    Footer footer;
    Header header;

    @BeforeTest
    public void openPage() {
        open(HOME_PAGE);
        footer = new Footer();
        header = new Header();
    }

    @AfterTest
    public void cleanup() {
        footer.resetApp();
    }

    @Test
    public void verify_footer_text() {
        assertTrue(footer.isFooterTextDisplayed(), "Build info must be displayed in the footer");
        assertEquals(footer.footerHomepageRedirectURL, HOME_PAGE, "Clicking the footer text will redirect" +
                "to the homepage");
    }

    @Test
    public void verify_question_mark_opens_help_modal() {
        assertTrue(footer.isQuestionMarkDisplayed(), "The question mark has to be displayed in the footer");
        footer.clickQuestionMark();
        assertTrue(header.isModalOpen(), "After clicking the question mark, the modal opens up");
        assertEquals(footer.helpModalHeaderText(), HELP_MODAL_HEADER_TEXT, "After clicking the question mark, " +
                "the help modal will be displayed with the right header");
    }

    @Test
    public void verify_reset_application_button() {
        assertTrue(footer.isResetIconDisplayed(), "The reset application button has to be displayed in the footer");
        assertEquals(footer.resetApplicationURL, BASE_URL, "After pressing the reset button, we are redirected " +
                "to the home page and the application has to be reset");
    }
}
