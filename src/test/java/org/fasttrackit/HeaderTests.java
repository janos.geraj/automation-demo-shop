package org.fasttrackit;

import org.fasttrackit.pages.Header;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.*;
import static org.fasttrackit.DataSource.*;
import static org.testng.Assert.*;

@Test
public class HeaderTests {

    Header header;

    @BeforeTest
    public void openPage() {
        open(HOME_PAGE);
        header = new Header();
    }

    @AfterTest
    public void cleanup() {
        header.resetApp();
    }

    @Test
    public void verify_brand_logo() {
        assertTrue(header.isBrandLogoDisplayed(), "Logo element exsits and is displayed in page header");
        assertEquals(header.homepageRedirectURL, HOME_PAGE, "Clicking the band logo redirects to the homepage");
    }

    @Test
    public void verify_shopping_cart_icon() {
        assertTrue(header.isShoppingCartIconDisplayed(), "Shopping cart icon exists and is displayed in page header");
        assertEquals(header.shoppingCartRedirectURL, CART_PAGE, "Clicking the shopping cart redirects to the cart page");
    }

    @Test
    public void verify_header_wishlist_icon() {
        assertTrue(header.isHeaderWishlistIconDisplayed(), "Wishlist icon exists and is displayed in page header");
        assertEquals(header.headerWishlistIconRedirectURL, WISHLIST_PAGE, "Clicking the wishlist icon from the" +
                "header redirects to the wishlist page");
    }

    @Test
    public void verify_greetings_message_is_welcome() {
        assertTrue(header.isGreetingsMessageDisplayed(), "Welcome message must be displayed in the header");
        assertEquals(header.greetingsMessage(), HELLO_GUEST_GREETINGS_MESSAGE, "Welcome message is displayed");
    }

    @Test
    public void verify_login_button_opens_login_modal_and_close() {
        assertTrue(header.isLoginButtonDisplayed(), "Login button must be displayed in the header");
        header.clickLoginButton();
        assertTrue(header.isModalOpen(), "The modal opened up");
        assertEquals(header.modalHeaderText(), LOGIN_MODAL_HEADER_TEXT, "After clicking the login button, the " +
                "login modal will be displayed, with the correct header");
    }

    @Test
    public void verify_if_cart_quantity_updates_upon_adding_products(){
        AWESOME_GRANITE_CHIPS_PRODUCT.addToShoppingCart();
        assertEquals(header.shoppingCartProductCount(), "1", "After adding a product to the shopping cart," +
                "the item count is updated");
    }

    @Test
    public void verify_if_wishlist_quantity_updates_upon_adding_products(){
        AWESOME_GRANITE_CHIPS_PRODUCT.addToWishlist();
        assertEquals(header.wishlistProductCount(), "1", "After adding a product to the shopping cart," +
                "the item count is updated");
    }
}
