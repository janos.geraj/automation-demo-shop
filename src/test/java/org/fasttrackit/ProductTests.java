package org.fasttrackit;

import org.fasttrackit.pages.BasePage;
import org.fasttrackit.pages.HomePageContent;
import org.fasttrackit.pages.Product;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.open;
import static org.fasttrackit.DataSource.*;
import static org.testng.Assert.*;

@Test
public class ProductTests extends BasePage {

    HomePageContent homePageContent;
    Product product;

    @BeforeTest
    public void openPage() {
        open(HOME_PAGE);
        homePageContent = new HomePageContent();
        product = new Product("3", "15.99");
    }

    @AfterTest
    public void cleanup() {
        homePageContent.resetApp();
    }

    @Test(dataProvider = "getProductData", dataProviderClass = DataSource.class)
    public void verify_Products_on_homepage(Product product) {
        assertTrue(product.isTitleDisplayed(), "Product " + product.getProductId() + " is displayed on page");
        assertEquals(product.url(), HOME_PAGE + product.getExpectedUrl(), "Product " + product.getProductId() + " page is opened");
        assertEquals(product.getPrice(), product.getExpectedPrice(), "Product " + product.getProductId() + " price is" + product.getExpectedPrice());
    }
}
