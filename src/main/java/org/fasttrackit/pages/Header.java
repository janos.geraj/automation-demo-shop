package org.fasttrackit.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class Header extends BasePage {

    /**
     * Selector collection
     */

    private final SelenideElement brandLogo = $(".navbar-brand .fa-shopping-bag");
    private final SelenideElement shoppingCartIcon = $(".fa-shopping-cart");
    private final SelenideElement greeting = $(".navbar-text > span");
    private final SelenideElement headerWishlistIcon = $(".navbar-text .fa-heart");
    private final SelenideElement loginIcon = $(".fa-sign-in-alt");
    private final SelenideElement modalBody = $(".modal-content");
    private final SelenideElement loginModal = $(".modal-title ");
    private final SelenideElement shoppingCartBadge = $("[data-icon=shopping-cart]~.shopping_cart_badge");
    private final SelenideElement wishlistIconBadge = $("[data-icon=heart]~.shopping_cart_badge");

    /**
     * Content
     */

    public String homepageRedirectURL = brandLogo.parent().getAttribute("href");

    public String shoppingCartRedirectURL = shoppingCartIcon.parent().getAttribute("href");

    public String headerWishlistIconRedirectURL = headerWishlistIcon.parent().getAttribute("href");

    public String greetingsMessage() {
        return greeting.getText();
    }

    public String modalHeaderText() {
        return loginModal.getText();
    }

    public String shoppingCartProductCount(){
        return shoppingCartBadge.getText();
    }

    public String wishlistProductCount(){
        return wishlistIconBadge.getText();
    }

    /**
     * Verifiers
     */

    public boolean isBrandLogoDisplayed() {
        return brandLogo.exists() && brandLogo.isDisplayed();
    }

    public boolean isShoppingCartIconDisplayed() {
        return shoppingCartIcon.exists() && shoppingCartIcon.isDisplayed();
    }

    public boolean isHeaderWishlistIconDisplayed() {
        return headerWishlistIcon.exists() && headerWishlistIcon.isDisplayed();
    }

    public boolean isGreetingsMessageDisplayed() {
        return greeting.exists() && greeting.isDisplayed();
    }

    public boolean isLoginButtonDisplayed() {
        return loginIcon.exists() && loginIcon.isDisplayed();
    }

    public boolean isModalOpen(){
        return modalBody.exists();
    }

    /**
     * Actions
     */

    public void clickLoginButton() {
        loginIcon.click();
    }
}