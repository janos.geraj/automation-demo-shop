package org.fasttrackit.pages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class HomePageContent extends BasePage {

    /**
     * Selector collection
     */

    public static final SelenideElement searchField = $("#input-search");
    public static final SelenideElement searchButton = $("form .btn ");
    public static final SelenideElement sortElement = $(".sort-products-select ");
    public static final ElementsCollection allProducts = $$(".card");
    public static final SelenideElement lowToHighSortOption = $("option[value=lohi]");

    /**
     * Content
     */

    public String searchFieldPlaceholder() {
        return searchField.getAttribute("placeholder");
    }

    public String searchButtonLabel() {
        return searchButton.getText();
    }

    public String defaultSorting() {
        return sortElement.getAttribute("selectedIndex");
    }

    public String firstProduct() {
        return allProducts.first().getText();
    }


    /**
     * Verifiers
     */

    public boolean isSearchFieldDisplayed() {
        return searchField.exists() && searchField.isDisplayed();
    }

    public boolean isSearchButtonDisplayed() {
        return searchButton.exists() && searchButton.isDisplayed();
    }

    public boolean isSortElementDisplayed() {
        return sortElement.exists() && sortElement.isDisplayed();
    }

    /**
     * Actions
     */

    public void clickSort() {
        sortElement.click();
    }

    public void selectLowToHighSort() {
        lowToHighSortOption.click();
    }
}
