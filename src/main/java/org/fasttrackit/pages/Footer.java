package org.fasttrackit.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class Footer extends BasePage{

    /**
     * Selector Collection
     */

    private final SelenideElement footerText = $(".nav-link");
    private final SelenideElement questionMark = $(".fa-question");
    private final SelenideElement helpModal = $(".modal-title");
    private final SelenideElement resetApplication = $(".fa-undo");

    /**
     * Content
     */

    public String footerHomepageRedirectURL = footerText.getAttribute("href");

    public String helpModalHeaderText(){
        return helpModal.getText();
    }

    public String resetApplicationURL = resetApplication.parent().getAttribute("baseURI");

    /**
     * Verifiers
     */

    public boolean isFooterTextDisplayed(){
        return footerText.exists() && footerText.isDisplayed();
    }

    public boolean isQuestionMarkDisplayed(){
        return questionMark.exists() && questionMark.isDisplayed();
    }

    public boolean isResetIconDisplayed(){
        return resetApplication.exists() && resetApplication.isDisplayed();
    }

    /**
     * Actions
     */

    public void clickQuestionMark(){
        questionMark.click();
    }
}
