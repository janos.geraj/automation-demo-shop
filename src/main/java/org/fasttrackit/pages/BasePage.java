package org.fasttrackit.pages;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class BasePage {
    private final SelenideElement closeModal = $(".close");

    public void refreshPage(){
        Selenide.refresh();
    }

    public void resetApp(){
        if(closeModal.exists()){
            closeModal.click();
        }
    }
}
