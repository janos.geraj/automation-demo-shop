package org.fasttrackit.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static java.lang.String.format;

public class Product extends BasePage {

    private final SelenideElement title;
    private final SelenideElement price;
    private final String expectedUrl;
    private final String expectedPrice;
    private final String productId;

    public Product(String productId, String price) {
        this.productId = productId;
        this.expectedPrice = price;
        this.title = $(format("a[href='#/product/%s']", productId));
        this.expectedUrl = format("#/product/%s", productId);
        this.price = title.parent().parent().$(".card-footer .card-text");
    }

    /**
     * Content
     */

    public String getProductId() {
        return productId;
    }

    public String url() {
        return title.getAttribute("href");
    }

    public String getExpectedUrl() {
        return this.expectedUrl;
    }

    public String getExpectedPrice() {
        return this.expectedPrice;
    }

    public String getPrice() {
        return this.price.getText();
    }

    public String getTitle() {
        return this.title.getText();
    }
    /**
     * Verifiers
     */

    public boolean isTitleDisplayed() {
        return this.title.exists() && this.title.isDisplayed();
    }

    /**
     * Actions
     */

    public void addToShoppingCart() {
        SelenideElement parent = title.parent().parent();
        SelenideElement addToShoppingCartIcon = parent.$("[data-icon=cart-plus]");
        addToShoppingCartIcon.click();
    }

    public void addToWishlist(){
        SelenideElement parent = title.parent().parent();
        SelenideElement addToWishlistIcon = parent.$("[data-icon='heart']");
        addToWishlistIcon.click();
    }
}
