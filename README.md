# Automation Demo Shop Test Automation Framework
        The Automation Demo Shop - project represents a test automation project 
    for the final exam. The website which was provided by FastTrackIt, mimics 
    an online shop based on which the automation test were implemented in the 
    Java programming language, while using different frameworks to make testing 
    more efficient.

## This is the final Project of Geraj Janos within the FastTrackIt test automation course.

### Tech stack used:
    - Java JDK-17
    - Maven v4.0.0
    - Selenium 
    - Selenide v6.4.0
    - Allure 
### Developed on the following system environment: 
    - Operation system: Ubuntu 20.04.4 LTS 64-bit (GNOME Version: 3.36.8)
    - IDE: IntellyJ IDEA 2021.3.3 Build #IU-213.7172.25, built on March 15, 2022
    - Browsers: 
        1. Google Chrome: Version 100.0.4896.60 (Official Build) (64-bit)
        2. Mozilla Firefox for Ubuntu 98.0.2 (64-bit)
### Tested site: 
    URL: https://fasttrackit-test.netlify.app/#/

## How to run the test suite
    - git clone https://gitlab.com/janos.geraj/automation-demo-shop.git
    - run automation-demo-shop/DemoShopTestSuite.xml from IntelliJ

### Page objects tested:
    - Header
    - Page content
    - Products
    - Footer

